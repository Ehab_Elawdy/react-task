import React, { Component } from "react";
import PropTypes from 'prop-types';
import { BrowserRouter , Route } from "react-router-dom";

// We want to update the title present in the Header 
// everytime we navigate to a new page. 
// Is passing the _setTitle method with the props the way to go?

export default class Question3 extends Component {
  constructor(props) {
    super(props);
    this._setTitle = this._setTitle.bind(this);
    this.state = { page : "" }
    console.log(this.props);
    
  }

    _setTitle(title) {
      // get title here 
      this.setState({ page : title });
    }

  render() {
    let pageTitle = this.state.page;
    return ( 
      <div> 
        <div className="container">
        {/* the page title form the childs Component */}
        <Header  title={ pageTitle }/>
          <BrowserRouter>
            <Route path="/profile" render={() =>  <Profile  setTitle={this._setTitle}/> }  /> 
            <Route path="/projects" render={() =>  <Projects  setTitle={this._setTitle}/> }  /> 
            <Route path="/projects-form" render={() =>  <ProjectForm  setTitle={this._setTitle}/> }  /> 
            <Route path="/translators" render={() =>  <Translators  setTitle={this._setTitle}/> }  /> 
          </BrowserRouter>
        <Footer />
        </div>
      </div>
    );
   }
}

  export class Header extends Component {

    render() { 
        const { title } = this.props;
        return ( 
          <React.Fragment>
              <h1>Header</h1>
              <h2>{title}</h2> 
              {/* the title form the Parent Component */}
          </React.Fragment>
        );
      }
  }

  export class Footer extends Component { 
    render() {
      return (
        <p>Footer Section</p>
      );
    }
  }

  export class Profile extends Component {
    constructor(props) {
      super(props);
      }

      componentWillMount() {
        this.props.setTitle('Profile');
      }

      render() {
        return (
          <h3>Profile Page</h3>
        );
      }
  }



  export class Projects extends Component { 
    constructor(props) {
      super(props);
      }
      componentWillMount() {
        this.props.setTitle('Projects');
      }

    render() {
      return (
        <h3>Projects Page</h3>
      );
    }
  }



  export class ProjectForm extends Component { 
    constructor(props) {
      super(props);
      }

      componentWillMount() {
        this.props.setTitle('New Project');
      }


     render() {
      return (
        <h3>Project Form Page</h3>
      );
    }
  }

  export class Translators extends Component { 
    constructor(props) {
      super(props);
      }

      componentWillMount() {
        this.props.setTitle('Translators');
      }

    render() {
      return (
        <h3>Translators Page</h3>
      );
    }
  }
