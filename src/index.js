import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Test from './Questions7';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css'



ReactDOM.render(<Test />, document.getElementById('root'));
registerServiceWorker();
