// Here is an ES5 component. Translate it to ES6. // Then render it in the target container with // id 'app'.
// <!DOCTYPE html> // <html> // <body> // <div id="app"></div> // </body> // </html>
// var React = require("react");
// var SearchFilter = React.createClass({
//   getInitialState: function() {
//     return { visible: false };
//   },
//   handleClick: function() {
//     this.setState({ visible: true });
//   },
//   render: function() {
//     return (
//       <div className="search">
//         {" "}
//         <i className="icon icon-search" onClick={this.handleClick} />{" "}
//         <p> Is the filter visible? {this.state.visible} </p>{" "}
//       </div>
//     );
//   }
// });

// The React Component in ES6

import React, { Component } from "react";
ReactDOM.render(class SearchFilter extends Component {

  state = {
    visible: false 
  };

  handleClick = (e) => {
    this.setState({ visible: true });
  }
  render() {
    return (
      <div className="search">
        <i className="icon icon-search" onClick={this.handleClick} />
        <p> Is the filter visible? {this.state.visible} </p>
      </div>
    );
  }
}, document.getElementById('root'));