// Component lifecycle:
// if we had to fetch the comments from a remote server,

// when would we perform that action?
//  //////////// Answer :
//  There are two common places to fetch the data  (componentDidMount() - componentWillMount()) and its happen after initial render 

// What's the name of the lifecycle method below?
// componentDidMount();

// What's the problem with the implementation below?
// the componentXXX function makes a problem because the lifecycle componentDidMount() will do the fetch data and change 
// the component's state itself .














import React, { Component } from "react";

export class CommentBox extends Component {
  constructor(props) {
    super(props);
    this.state = { comments : []}
  }

  // ...
  _fetchComments() {
    // We don't care about this method implementation
    // However it's worth noting that it updates the component's state
  }
  // Voluntarily obfuscated
  // componentXXX() {
  //   setInterval(() => this._fetchComments(), 5000);
  // }

  componentDidMount = () => {
    this._fetchComments();
  };

  render() {
    // ...
  }
}
