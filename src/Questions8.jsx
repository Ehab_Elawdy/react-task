// What's the problem with that implementation?
// we can't use this keyword before super fun because the Test class is extends from Component .
// How can we easily fix that problem?
// we add Super() in constructor  function before using this . 


import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Test extends Component {
  constructor() {
    super();
    this.state = {
      value: { foo: "bar" }
    }; 
    this.onClick = this.onClick.bind(this);
  }
  onClick() {
    let value = this.state.value;
    value.foo += "bar";
    this.setState({ value: value });
  }
  render() {
    console.log(this.state.value)
    return (
      <div>
        <InnerComponent value={this.state.value} />
        <a onClick={this.onClick}>Click me</a>
      </div>
    );
  }
}

export class InnerComponent extends Component {
  static propTypes = { value: PropTypes.object.isRequired };
  render() {
    return (
        <h4>Hello</h4>
    );
  }
}
