// How can you refactor this class to manipulate the DOM the React way?

import React, { Component } from "react";

export default class CommentBox extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }
  showComments() {
    this.myRef.current.style.display = "block";
  }
  hideComments() {
    this.myRef.current.style.display = "none";
  }
  render() {
    const { comments } = this.props;
    return (
      <div>
        <button onClick={this.showComments.bind(this)}>Show comments</button>
        <button onClick={this.hideComments.bind(this)}>Hide comments</button>
        <div ref={this.myRef} >{comments}</div>
      </div>
    );
  }
}
