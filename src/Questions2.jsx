import React, { Component } from "react";
// import PropTypes from "prop-types";
import { BrowserRouter , Link } from "react-router-dom";

export default class Projects extends Component {
  //   static propTypes = { projects: PropTypes.array.isRequired };
  //* I don't need to import protypes in my component 
  //** We have to Import BrowserRouter and insert to the code make it as a countainer for Link Tag.
  //*** We can assign to the this.props dirctly to do the map and some refactor.
  render() {
    return (
      <div className="projects container">
        <BrowserRouter>
          <Link to="/projects/new" className="btn">
            New project
          </Link>
        </BrowserRouter>
            { this.props.projects.map((project) => (
                <div key={project.id}>
                    <span className="language origin">
                    {project.languageFrom}
                    </span>
                    <span className="language destination"> {project.languageTo} </span>
                </div>
            ))}
      </div>
    );
  }
}
