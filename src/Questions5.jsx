// Complete this piece of code to actually create the comment // when the form is submitted
import React, { Component } from "react";

export default class CommentBox extends Component {
  constructor(props) {
    super(props);
    this.state = { comments : [{author:'' , comment : ''}]};
    this._addComment = this._addComment.bind(this);
    this.comments = [];
  }

  _addComment = comment => {
    // Implement this method to mutate the state and add comments
    this.comments.push(comment);
    this.setState({ comments: this.comments });
  };

  componentDidMount = () => {
    this._addComment({author:'' , comment : ''});
  };

  renderComments() {
    return this.state.comments.map(comment => {
        return (
                <li key={comment.author}>{comment.author}</li>
        );
    })
}

  render() {
    // Voluntarily truncated return ( <div> <CommentForm addComment={this._addComment()} /> </div> ); }
    const comments = this.state.comments || [ ]
    console.log(comments);

    return (
      <div>
        <ul>
        {this.renderComments()}
        </ul>
        <CommentForm addComment={this._addComment} />
      </div>
    );
  }
}


export class CommentForm extends Component {
  constructor() {
    super();
    this._handleSubmit = this._handleSubmit.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this.state = { author: "", comment: "" };
  }

  _handleChange(event) {
    const target = event.target;
    const name = target.name;
    this.setState({
      [name]: target.value
    });
  }

  _handleSubmit(event) {
    // Write the content of this method
    // this.setState({author: event.target.value  , comment});
    event.preventDefault();
    this.props.addComment(this.state);
    this.setState({ author: "", comment: "" });
  }

  render() {
    // You can modify the returned value return ( <form role="form" onSubmit={this._handleSubmit()} className="form comment"> <input placeholder="Name:" /> <textarea placeholder="Comment:" /> </form> ); }
    return (
      <form onSubmit={this._handleSubmit} className="form comment">
        <input
          placeholder="Name:"
          name="author"
          value={this.state.author}
          onChange={this._handleChange}
        />
        <textarea
          placeholder="Comment:"
          name="comment"
          value={this.state.comment}
          onChange={this._handleChange}
        />
        <input type="submit" value="submit" />
      </form>
    );
  }
}